#-------------------------------------------------
#
# Project created by QtCreator 2016-07-11T18:07:39
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Texter
TEMPLATE = app

MOC_DIR = .m
OBJECTS_DIR = .o


SOURCES += src/main.cpp\
	src/MainWindow.cpp

HEADERS  += src\MainWindow.h
